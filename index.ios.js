var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;
import LogMenu from './src/ios/LogMenu';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Statusbar,
  NavigatorIOS,
} from 'react-native';

class vtask extends Component {
  render() {
    return (
      <NavigatorIOS navigationBarHidden={true} ref="nav" initialRoute={{title: "LogMenu", component: LogMenu, wrapperStyle: {backgroundColor: 'transparent'}}} style={{flex: 1}}/>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgb(56, 22, 34)',
  },
  title: {
    fontSize: 40,
    color: 'white',
  },
  wrapper: {
    width: deviceWidth,
    height: deviceHeight,
    flex: 1,
  },
  trans: {
    flex: 1,
    width: deviceWidth,
    height: deviceHeight,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(38, 38, 38, 0.52)',
  },
});

AppRegistry.registerComponent('vtask', () => vtask);
