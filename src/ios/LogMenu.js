var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;
import Icon from 'react-native-vector-icons/Ionicons';
import RegistrationForm from './registrationForm';
import ForgotPassword from './forgotPassword';
import Menu from './menu';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  AlertIOS,
} from 'react-native';
class LogMenu extends Component {
  constructor(props){
    super(props);
      this.state={
        onLogged: false,
        onShowLogMenu: false,
        isSignedIn: false,
        onSignIn: false,
        onRedirect: false,
        onShowSignUp: false,
        onShowRegister: false,
        onShowForgotPassword: false,
        username: 'user@task.com',
        password: '12345',
        onPressLinkRegister: false,
        linkColor: 'rgb(82, 233, 206)',
        defaultColor1: 'rgb(255, 255, 255)',
        defaultColor2: 'rgb(255, 255, 255)',
    };
    this.onLogged = this.onRedirect.bind(this);
    this.onShowRegister = this.onShowRegister.bind(this);
    this.onShowForgotPassword = this.onShowForgotPassword.bind(this);
    this.onPressLinkRegister = this.onPressLinkRegister.bind(this);
    this.onRedirect = this.onRedirect.bind(this);
  }
onShowRegister(){
  console.log('register');
  this.props.navigator.push({component: RegistrationForm,})
}
onShowForgotPassword(){
  console.log('forgot password');
  this.props.navigator.push({component: ForgotPassword,})
}
onRedirect(){
  console.log('log success');
}
onSignIn(){
  if(this.state.username == 'user@task.com' && this.state.password == '12345'){
    console.log('log success');
    this.props.navigator.push({component: Menu,})
  }
  else{
    console.log('failed');
  }
}
onPressLinkRegister(){
 this.setState({ defaultColor1: this.state.linkColor});
}
onPressReleaseRegister(){
 this.setState({ defaultColor1: 'rgb(255, 255, 255)',
                   onPressLinkRegister: false,});
}
onPressLinkForgot(){
 this.setState({ defaultColor2: this.state.linkColor});
}
onPressReleaseForgot(){
  this.setState({ defaultColor2: 'rgb(255, 255, 255)',
                    onPressLinkForgot: false,});
}

  render() {
    return (
      <View style={styles.container}>
       <StatusBar backgroundColor="black" barStyle="light-content"/>
       <Image style={styles.wrapper} source={require('../img/blurred2.jpg')} />
       <View style={styles.trans}>
        <Text style={styles.title}>VeeTask</Text>
        <View style={styles.contentHolder}>
          <View style={styles.nameHolder}>
           <View style={[styles.seperator,{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}]}>
            <Icon name="ios-mail-outline" color="white" style={[styles.iconHolder, {backgroundColor: 'transparent'}]} size={30} />
            <TextInput placeholder="e-mail eg. user@vtask.com" style={styles.txtName} onChangeText={(username)=>this.setState({username})} value={this.state.username}/>
           </View>
           <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
            <Icon name="ios-lock-outline" color="white" style={[styles.iconHolder, {backgroundColor: 'transparent'}]} size={30} />
            <TextInput placeholder="password eg. password1234" style={styles.txtName} secureTextEntry={true} onChangeText={(password)=>this.setState({password})} value={this.state.password}/>
           </View>
          </View>
          <TouchableOpacity onPress={this.onSignIn.bind(this)}>
            <View style={[styles.button, {backgroundColor: 'rgb(82, 233, 206)'}]}>
              <Text style={styles.buttonText}>SIGN IN</Text>
            </View>
          </TouchableOpacity>
          <View style={styles.extraMenuHolder}>
           <TouchableWithoutFeedback onPressIn={this.onPressLinkRegister.bind(this)} onPressOut={this.onPressReleaseRegister.bind(this)} onPress={this.onShowRegister.bind(this)}>
            <View>
             <Text style={[styles.txtExtra,{color: this.state.defaultColor1}]}>
               Register
              </Text>
            </View>
           </TouchableWithoutFeedback>
           <TouchableWithoutFeedback  onPressIn={this.onPressLinkForgot.bind(this)} onPressOut={this.onPressReleaseForgot.bind(this)} onPress={this.onShowForgotPassword.bind(this)}>
            <View>
             <Text style={[styles.txtExtra,{color: this.state.defaultColor2}]}>
               Forgot Password?
             </Text>
            </View>
           </TouchableWithoutFeedback>
          </View>
        </View>
       </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgb(56, 22, 34)',
  },
  title: {
    fontSize: deviceWidth*0.1,
    color: 'white',
  },
  wrapper: {
    width: deviceWidth,
    height: deviceHeight,
    flex: 1,
  },
  trans: {
    flex: 1,
    width: deviceWidth,
    height: deviceHeight,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(38, 38, 38, 0.52)',
  },
  button:{
    height: deviceHeight*0.08,
    width: deviceWidth*0.75,
    borderRadius: deviceWidth*0.015,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: deviceWidth*0.05,
    marginRight: deviceWidth*0.05,
  },
  contentHolder:{
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: deviceHeight*0.2,
  },
  buttonText:{
    color: 'rgb(255, 255, 255)',
    fontSize: deviceHeight*0.08/3,
  },
  txtName:{
    height: deviceHeight*0.08,
    width: deviceWidth*0.6,
    fontSize: deviceHeight*0.08/3.5,
    color:'rgb(16, 49, 55)',
    color: 'white'
  },
  nameHolder:{
    height: deviceHeight*0.15,
    width: deviceWidth*0.75,
    backgroundColor: 'white',
    borderRadius: deviceWidth*0.015,
    marginBottom: deviceHeight*0.02,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: 'white',
    marginTop: -deviceWidth*0.1,
  },
  seperator:{
    borderBottomWidth: 0.5,
    borderColor: 'rgb(136, 136, 136)',
    width:deviceWidth*0.7,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'white',
  },
  extraMenuHolder:{
    marginTop: deviceHeight*0.01,
    flexDirection: 'row',
    alignItems: 'center',
  },
  txtExtra:{
    color: 'rgb(255, 255, 255)',
    fontSize: deviceHeight*0.08/4,
    marginLeft: deviceHeight*0.07,
    marginRight: deviceHeight*0.08,
  },
  iconHolder:{
    marginRight: deviceHeight*0.08/3.5,
    marginLeft: deviceHeight*0.08/3,
  }
});

module.exports = LogMenu;
