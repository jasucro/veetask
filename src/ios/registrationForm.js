var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;
import Icon from 'react-native-vector-icons/Ionicons';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  AlertIOS,
} from 'react-native';
class Register extends Component {
  constructor(props){
    super(props);
      this.state={
        onLogged: false,
        onShowLogMenu: false,
        isSignedIn: false,
        onSignIn: false,
        onRedirect: false,
        onShowSignUp: false,
        onShowRegister: false,
        onShowForgotPassword: false,
        name: 'Juan dela Cruz',
        username: 'user@task.com',
        password: '12345',
        onPressLinkRegister: false,
        linkColor: 'rgb(82, 233, 206)',
        defaultColor1: 'rgb(255, 255, 255)',
        defaultColor2: 'rgb(255, 255, 255)',
    };
    this.onLogged = this.onRedirect.bind(this);
    this.onShowRegister = this.onShowRegister.bind(this);
    this.onShowForgotPassword = this.onShowForgotPassword.bind(this);
    this.onPressLinkRegister = this.onPressLinkRegister.bind(this);
    this.onRedirect = this.onRedirect.bind(this);
  }
  onShowRegister(){
    console.log('register');
  }
  onShowForgotPassword(){
    console.log('forgot password');
  }
  onRedirect(){
    console.log('log success');
  }
  onBack(){
    this.props.navigator.pop()
  }
  onSignUp(){


  }
  onPressLinkRegister(){
   this.setState({ defaultColor1: this.state.linkColor});
  }
  onPressReleaseRegister(){
   this.setState({ defaultColor1: 'rgb(255, 255, 255)',
                     onPressLinkRegister: false,});
  }


  render() {
    return (
      <View style={styles.container}>
       <StatusBar backgroundColor="black" barStyle="light-content"/>
       <Image style={styles.wrapper} source={require('../img/blurred2.jpg')} />
       <View style={styles.trans}>
       <View style={styles.navtop}>
            <TouchableOpacity onPress={this.onBack.bind(this)}>
              <View style={styles.btnBack}>
                <Icon name="ios-arrow-back-outline" color="white" size={deviceHeight*0.035} />
                <Text style={styles.navtxt}> Back </Text>
              </View>
          </TouchableOpacity>
        </View>
        <Text style={styles.title}>Register</Text>
        <View style={styles.contentHolder}>
          <View style={styles.nameHolder}>
           <View style={[styles.seperator, {flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}]}>
            <Icon name="ios-person-outline" color="white" style={[styles.iconHolder, {backgroundColor: 'transparent'}]} size={30} />
            <TextInput placeholder="name eg. Juan dela Cruz" style={styles.txtName} onChangeText={(name)=>this.setState({name})} value={this.state.name}/>
           </View>
          <View style={[styles.seperator, {flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}]}>
            <Icon name="ios-mail-outline" color="white" style={[styles.iconHolder, {backgroundColor: 'transparent'}]} size={30} />
            <TextInput placeholder="e-mail eg. user@vtask.com" style={styles.txtName} onChangeText={(username)=>this.setState({username})} value={this.state.username}/>
           </View>
           <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
            <Icon name="ios-lock-outline" color="white" style={[styles.iconHolder, {backgroundColor: 'transparent'}]} size={30} />
            <TextInput placeholder="password eg. password1234" style={styles.txtName} secureTextEntry={true} onChangeText={(password)=>this.setState({password})} value={this.state.password}/>
           </View>
          </View>
          <TouchableOpacity >
            <View style={[styles.button, {backgroundColor: 'rgb(82, 233, 206)'}]}>
              <Text style={styles.buttonText}>SIGN UP</Text>
            </View>
          </TouchableOpacity>
          <View style={styles.extraMenuHolder}>
           <TouchableWithoutFeedback onPressIn={this.onPressLinkRegister.bind(this)} onPressOut={this.onPressReleaseRegister.bind(this)} onPress={this.onShowRegister.bind(this)}>
            <View>
             <Text style={[styles.txtExtra,{color: this.state.defaultColor1}]}>
               Login
              </Text>
            </View>
           </TouchableWithoutFeedback>
          </View>
        </View>
       </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgb(56, 22, 34)',
  },
  title: {
    fontSize: deviceWidth*0.1,
    color: 'white',
  },
  wrapper: {
    width: deviceWidth,
    height: deviceHeight,
    flex: 1,
  },
  trans: {
    flex: 1,
    width: deviceWidth,
    height: deviceHeight,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(38, 38, 38, 0.52)',
  },
  navtop:{
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: deviceWidth,
    height: 25,
    backgroundColor: 'transparent',
    marginTop: 15,
    marginLeft: 1,
    padding: 4,
    flexDirection: 'row',
  },
  navtxt:{
    color: 'white',
    fontSize: deviceHeight*0.025,
    marginLeft: 4,
    marginTop: deviceHeight*0.0015,
  },
  btnBack:{
    flexDirection: 'row',
  },
  button:{
    height: deviceHeight*0.08,
    width: deviceWidth*0.75,
    borderRadius: deviceWidth*0.015,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: deviceWidth*0.05,
    marginRight: deviceWidth*0.05,
  },
  contentHolder:{
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: deviceHeight*0.2,
  },
  buttonText:{
    color: 'rgb(255, 255, 255)',
    fontSize: deviceHeight*0.08/3,
  },
  txtName:{
    height: deviceHeight*0.08,
    width: deviceWidth*0.6,
    fontSize: deviceHeight*0.08/3.5,
    color:'rgb(16, 49, 55)',
    color: 'white'
  },
  nameHolder:{
    height: deviceHeight*0.23,
    width: deviceWidth*0.75,
    backgroundColor: 'white',
    borderRadius: deviceWidth*0.015,
    marginBottom: deviceHeight*0.02,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: 'white',
    marginTop: -deviceWidth*0.1,
  },
  seperator:{
    borderBottomWidth: 0.5,
    borderColor: 'rgb(136, 136, 136)',
    width:deviceWidth*0.7,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'white',
  },
  extraMenuHolder:{
    marginTop: deviceHeight*0.02,
    flexDirection: 'row',
    alignItems: 'center',
  },
  txtExtra:{
    color: 'rgb(255, 255, 255)',
    fontSize: deviceHeight*0.08/4,
    marginLeft: deviceHeight*0.07,
    marginRight: deviceHeight*0.08,
  },
  iconHolder:{
    marginRight: deviceHeight*0.08/3.5,
    marginLeft: deviceHeight*0.08/3,
  }
});

module.exports = Register;
