var Dimensions = require('Dimensions');
var deviceWidth = Dimensions.get('window').width;
var deviceHeight = Dimensions.get('window').height;
import DrawerLayout from 'react-native-drawer-layout';
import Drawer from 'react-native-drawer';
import Icon from 'react-native-vector-icons/Ionicons';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  AlertIOS,
  TouchableHighlight,
  ScrollView,
} from 'react-native';
class Menu extends Component {
  constructor(props){
    super(props);
      this.state={
        onShowTasks: true,
        onShowUsers: false,
        onShowProjects: false,
        onShowSettings: false,
        colorNew: 'rgb(110, 221, 43)',
        colorInProgress: 'rgb(42, 231, 219)',
        colorRush: 'rgb(233, 48, 36)',
    };
    this.onShowTasks = this.onShowTasks.bind(this);
    this.onShowUsers = this.onShowUsers.bind(this);
    this.onShowProjects = this.onShowProjects.bind(this);
    this.onShowSettings = this.onShowSettings.bind(this);
  }
  onBack(){
    this.props.navigator.pop()
  }
  onShowTasks(){
    this.setState({
      onShowTasks: true,
      onShowUsers: false,
      onShowProjects: false,
      onShowSettings: false,
    });
    this.drawer.closeDrawer();
  }
  onShowUsers(){
    this.setState({
      onShowUsers: true,
      onShowTasks: false,
      onShowProjects: false,
      onShowSettings: false,
    });
    this.drawer.closeDrawer();
  }
  onShowProjects(){
    this.setState({
      onShowProjects: true,
      onShowTasks: false,
      onShowUsers: false,
      onShowSettings: false,
    });
    this.drawer.closeDrawer();
  }
  onShowSettings(){
    this.setState({
      onShowSettings: true,
      onShowTasks: false,
      onShowProjects: false,
      onShowUsers: false,
    });
    this.drawer.closeDrawer();
  }
  renderTasks(){
    if(this.state.onShowTasks){
      return(
         <View>
            <View style={styles.taskContainer}>
              <View style={styles.taskContent}>
                <View style={styles.taskBadge}/>
                  <View style={styles.taskHolder}>
                    <Text style={styles.txtTask}>Create a button </Text>
                  </View>
                <TouchableOpacity>
                <View style={styles.taskProject}>
                  <Text style={{color: 'white'}}> Neer </Text>
                </View>
                </TouchableOpacity>
                <View style={styles.taskTime}>
                  <Text style={{color: 'white'}}> Today </Text>
                </View>
              </View>
            </View>

            <View style={styles.taskContainer}>
              <View style={styles.taskContent}>
                <View style={styles.taskBadge}/>
                  <View style={styles.taskHolder}>
                    <Text style={styles.txtTask}>Create a button </Text>
                  </View>
                <TouchableOpacity>
                <View style={styles.taskProject}>
                  <Text style={{color: 'white'}}> Neer </Text>
                </View>
                </TouchableOpacity>
                <View style={styles.taskTime}>
                  <Text style={{color: 'white'}}> Today </Text>
                </View>
              </View>
            </View>
          </View>
      );
    }
  }
  renderProjects(){
    if(this.state.onShowProjects){
      return(<View>
              <Text> PROJECTS </Text>
            </View>);
    }
  }
  renderUsers(){
    if(this.state.onShowUsers){
      return(
        <View>
          <Text> USERS </Text>
        </View>);
    }
  }
  renderSettings(){
    if(this.state.onShowSettings){
      return(
        <View>
         <Text> SETTINGS </Text>
        </View>);
    }
  }
  render() {
    return (
        <View style={styles.container}>
          <StatusBar backgroundColor="black" barStyle="light-content" hidden={true}/>
          <View style={styles.trans}>
             <View style={{position: 'absolute', top: 0, bottom: 0, left: 0, right: 0}}>
               <DrawerLayout
                drawerBackgroundColor="white"
                drawerWidth={deviceWidth*0.9}
                ref={(drawer) => { return this.drawer = drawer  }}
                keyboardDismissMode="on-drag"
                renderNavigationView={() =>
                <View style={{flexDirection: 'column'}}>
                   <View style={styles.menuHeader}>
                   </View>
                   <TouchableOpacity onPress={this.onShowTasks.bind(this)}>
                    <View style={styles.menuContent}>
                      <View style={styles.menuSub}>
                        <Icon name="ios-list-box-outline" color="rgb(21, 143, 147)" style={[styles.iconHolder, {backgroundColor: 'transparent'}]} size={deviceHeight*0.05} />
                        <Text style={styles.txtMenu}>Tasks</Text>
                      </View>
                    </View>
                   </TouchableOpacity>
                   <TouchableOpacity onPress={this.onShowProjects.bind(this)}>
                    <View style={styles.menuContent}>
                      <View style={styles.menuSub}>
                      <Icon name="ios-briefcase-outline" color="rgb(21, 143, 147)" style={[styles.iconHolder, {backgroundColor: 'transparent'}]} size={deviceHeight*0.05} />
                      <Text style={styles.txtMenu}>Projects</Text>
                      </View>
                    </View>
                   </TouchableOpacity>
                   <TouchableOpacity onPress={this.onShowUsers.bind(this)}>
                    <View style={styles.menuContent}>
                      <View style={styles.menuSub}>
                      <Icon name="ios-person-outline" color="rgb(21, 143, 147)" style={[styles.iconHolder, {backgroundColor: 'transparent'}]} size={deviceHeight*0.05} />
                      <Text style={styles.txtMenu}>Users</Text>
                      </View>
                    </View>
                   </TouchableOpacity>
                   <TouchableOpacity onPress={this.onShowSettings.bind(this)}>
                    <View style={styles.menuContent}>
                      <View style={styles.menuSub}>
                      <Icon name="ios-settings-outline" color="rgb(21, 143, 147)" style={[styles.iconHolder, {backgroundColor: 'transparent'}]} size={deviceHeight*0.05} />
                      <Text style={styles.txtMenu}>Settings</Text>
                      </View>
                    </View>
                   </TouchableOpacity>
                </View>
                }>
                <View style={styles.navtop}>
                 <View style={styles.btnBack}>
                  <TouchableOpacity onPress={() => this.drawer.openDrawer()}>
                    <Icon name="ios-menu" color="white" size={deviceHeight*0.055} />
                  </TouchableOpacity>
                  <View style={styles.navTitle}>
                    <Text style={styles.navtxt}>VeeTask</Text>
                  </View>
                  <View style={[styles.navTitle,{marginTop: deviceHeight*0.005, marginLeft: deviceWidth*0.32}]}>
                   <Icon name="md-person" color="white" size={deviceHeight*0.045} />
                  </View>
                 </View>
                </View>
                <ScrollView style={styles.scroll}>
                {this.renderTasks()}
                {this.renderUsers()}
                {this.renderProjects()}
                {this.renderSettings()}
                </ScrollView>
               </DrawerLayout>
             </View>
          </View>
       </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: deviceWidth,
    height: deviceHeight,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(38, 38, 38, 0.52)',
    backgroundColor: 'rgb(240, 240, 240)',
    backgroundColor: 'rgb(228, 228, 228)'
  },
  title: {
    fontSize: deviceWidth*0.1,
    color: 'white',
  },
  wrapper: {
    width: deviceWidth,
    height: deviceHeight,
    flex: 1,
  },
  scroll:{
    marginTop: deviceHeight*0.07,
  },
  trans: {
    flex: 1,
    width: deviceWidth,
    height: deviceHeight,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(38, 38, 38, 0.52)',
    backgroundColor: 'rgb(240, 240, 240)',
    backgroundColor: 'rgb(228, 228, 228)'
  },
  navtop:{
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: deviceWidth,
    height: deviceHeight*0.07,
    backgroundColor: 'rgb(29, 233, 233)',
    marginLeft: 0,
    padding: 4,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderColor: 'rgba(255, 255, 255, 0.21)',
    borderRadius: 1,
    backgroundColor: 'rgb(65, 232, 255)',
    borderColor: 'rgb(171, 171, 171)'
  },
  navTitle:{
    alignSelf: 'center',
    marginLeft: deviceWidth*0.28,
    marginTop: -deviceHeight*0.005
  },
  navtxt:{
    color: 'white',
    fontSize: deviceHeight*0.035,
  },
  btnBack:{
    flexDirection: 'row',
    marginLeft:deviceWidth*0.01,
  },
  menuContent:{
    width:deviceWidth*0.9,
    height: deviceHeight*0.1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
    borderColor: 'rgb(21, 143, 147)',
    borderBottomWidth: 0.25,
    marginTop: 1,
  },
  menuSub:{
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: deviceHeight*0.02
  },
  menuHeader:{
    width:deviceWidth*0.9,
    height: deviceHeight*0.3,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgb(246, 246, 246)',
    borderColor: 'rgb(21, 143, 147)',
    borderBottomWidth: 0.6,
    marginTop: 1,
  },
  txtMenu:{
    fontSize: deviceHeight*0.025,
    color: 'rgb(21, 143, 147)',
  },
  iconHolder:{
    marginRight: deviceHeight*0.08/3.5,
    marginLeft: deviceHeight*0.08/3,
  },
  taskContainer:{
    width: deviceWidth,
    backgroundColor: 'white',
    height: deviceHeight*0.1,
    borderBottomWidth: 0.5,
    borderTopWidth: 0.5,
    borderColor: 'rgb(162, 162, 162)',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: 10,
  },
  taskContent:{
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: deviceHeight*0.03
  },
  taskBadge:{
    backgroundColor: 'rgb(110, 221, 43)',
    width: deviceWidth*0.06,
    height: deviceWidth*0.06,
    borderRadius: deviceWidth*0.06/2,
    marginLeft: deviceWidth*0.05
  },
  taskProject:{
    width: deviceWidth*0.2,
    height: deviceHeight*0.05,
    backgroundColor: 'rgb(61, 213, 223)',
    borderRadius: 3,
    borderBottomWidth: 0.5,
    borderColor: '#777',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: deviceWidth*0.05
  },
  taskTime:{
    width: deviceWidth*0.2,
    height: deviceHeight*0.05,
    backgroundColor: 'rgb(230, 132, 132)',
    borderRadius: 3,
    borderBottomWidth: 0.5,
    borderColor: '#777',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: deviceWidth*0.05
  },
  taskHolder:{
  marginLeft: deviceWidth*0.05,
  width: deviceWidth*0.3,
  alignItems: 'center',
  justifyContent: 'center',
  },
  txtTask:{
  fontSize: deviceHeight*0.02,
  },
});

module.exports = Menu;
